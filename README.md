# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* B. Bajic, A. Suveer, A. Gupta, I.Pepic, J. Lindblad, N. Sladoje, and I.M. Sintorn, “Denoising of Short Exposure Transmission Electron Microscopy Images for Ultrastructural Enhancement”, accepted for publication in 15th IEEE International Symposium on Biomedcial Imaging (ISBI), 2018.
### How do I get set up? ###
* Run train.py to train models in five-fold validation
* Configuration: python, opencv, numpy,keras, tensorflow
* Dependencies :helper.py, architecture.py, preprocessing.py
* Run prediction.py to denoise images
### Who do I talk to? ###
* Repo owner or admin anindya.gupta@ttu.ee
