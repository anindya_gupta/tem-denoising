#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 10:50:56 2017

@author: anindya
"""

import os
import numpy as np
from helpers import *
import h5py
import pandas as pd
from keras.models import load_model
from preprocessing import *
from architecture import *



##===========initialization part========#
#
data_path=path=os.getcwd()+'/Data Denoising/HM/'
GT_path=os.getcwd()+'/Data Denoising/GT/'
winW,winH,step=[128,128,64]
fold_path=os.getcwd()+'/train_data/'
#===================for single image================+#
#stich_img,patches,patch_array, x_ind,y_ind=single_image_read(data_path,winW,winH,step,0)

#================For bulk traning data================+++#
#range1=0
#range2=2
#range3,range4,range5,range6=None,None,None,None
mode="train"
mode1="GT"
#
image_chunk(data_path,winW,winH,step,mode, range1=0,range2=3,range3=None,range4=None,range5=None,range6=None)

image_chunk(GT_path,winW,winH,step,mode1, range1=0,range2=3,range3=None,range4=None,range5=None,range6=None)


create_file_indices(mode)#return two csv files for training and testing for 5-fold validation 


cv_fold('train_split.csv') #reutrn 5 csv files each for training and validation data (five-fold validation)

batch=2
epoch=10
print ("data prepared..... for 5-fold cross-validation"+"\n")

mode2="DCNN" #"singlenet"#"multinet"#

for i in range(5):# 

     print ("\n"+"cross-validation..."+ str(i+1)+"\n")

     f_tr = (fold_path+'fold_tr'+str(i+1)+'.csv')
     f_val = (fold_path+'fold_val'+str(i+1)+'.csv')
     tmp = pd.read_csv(f_tr)
     tmp1 = pd.read_csv(f_val)
#     f_test= ('train_split.csv')


#===== data generator===============#

     print ("\n"+"Starting training..."+"\n")
  
     
     tr=data_Genrator(f_tr,winW, winH, 1, batch,mode=mode2)
     
     val_data=data_Genrator(f_tr,winW, winW, 1, batch,mode=mode2)#"multinet")

     nb_validation_samples =round(84035/3)#len(tmp1)#*2#840#00
     nb_train_samples=round(336140/3.5)#round(len(tmp))#*1.2)#24000#00
     
     
     Net=DCNN_training(tr,nb_train_samples,val_data,nb_validation_samples, epoch,batch,i)
     #multistrair_training(tr,nb_train_samples,val_data,nb_validation_samples, epoch,batch,i)
     
     print ("\n"+"log is saved as: " +str(mode)+'_log_'+str(i+1)+'.csv'+"\n")
     print ("model weights are saved as: " +str(mode)+"_weights_"+str(i+1)+".h5"+"\n")
     print ("model is saved as: " +str(mode)+"_model_"+str(i+1)+".h5"+"\n")
     
    #============for testing each model in 5 fold validation==== 
    # cvscores=[]
     f_test= (fold_path+'test_split.csv')
     data = pd.read_csv(f_test)
     ID1 = data.pop('h5_id')
     tmp=np.zeros((len(data), winW, winW, 1,))
     y = np.zeros((len(data), winW, winW, 1,))    
     #y1 = np.zeros((len(data), winW, winW, 1,))    
       
     for ii in range(len(data)):
     
                  
        data_1=[]
        label_1=[]
        fileName = os.getcwd()+'/train_data/'+"train.h5"
        f=h5py.File(fileName,'r')
        data_file= f['train']
        
        data_1.append(np.stack([data_file[ID1[ii]]],axis=-1))#.reshape((20,20,7))]
        
        
        fileName1 = os.getcwd()+'/train_data/'+"GT.h5"
        f1=h5py.File(fileName1,'r')
        label_file= f1['GT']                
        #tmp2.append(tmp1[:,:,:,ID1[ii]])
        label_1.append(np.stack([label_file[ID1[ii]]],axis=-1))#.reshape((20,20,7))]
        
        tp=np.asarray(data_1)
        tp1=np.asarray(label_1)
        
        
        tmp[i, :, :,:]=tp
        y[i, :, :,:]=tp1
        #y1[i, :, :,:]=tp1

     file_test_log=open(os.getcwd()+'/logs/'+'DCNN_test_log'+str(i+1)+'.csv','w')
     
     file_test_log.write(str('m_no')+ "," + str('mse')+ "\n")
     NET=load_model(os.getcwd()+'/trained_models/'+'DCNN_model_'+ str(i+1)+'.h5')
      
     scores = NET.evaluate(tmp,y, verbose=1)# [y,y1]
      
     print("on independent set "+"%s: %.4f" % (NET.loss, np.float32(scores)))
      
      #print("on independent set "+"%s: %.4f" % (NET.metrics_names[1], scores[1]))
      
     file_test_log.write(str(i+1)+','+ str(np.float32(scores))+"\n")
      
     file_test_log.close()
      
     #cvscores.append(np.float32(scores))
#===================data arrangement==================+#

                


