#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 10:50:56 2017

@author: anindya
"""
import os
os.makedirs(os.getcwd()+'/train_data',exist_ok=True)
import numpy as np
import PIL
#from skimage.util.shape import view_as_blocks
import matplotlib
import pandas as pd
import math
import h5py
#from skimage.transform import resize
import matplotlib.pyplot as plt
from preprocessing import preprocess, preprocesser
from sklearn.cross_validation import KFold
from PIL import Image
from keras.models import load_model
from architecture import *
seed = 7

#==========overlapping and non-overlapping patch extraction ========#
def extract_patch(im,winW, winH,step):#
    
    """
    sliding window to extract patches from the whole image of even sizes 
    i.e., 32X32, 64X64, 128X128, 256X256, 512X512, 1024X1024.....
    
    Recommended: for overalaping patches and non-overlapping patches
    
        Input:
            winW: window width 
            winH: window height
            step: pixel overlapping
            
        Output:
            win: list of all patches
            xx: indices of x dimension of all patches
            yy: indices of y dimension of all patches
                
            example: slide_patch,x_indices,y_indices=extract_patch(data_path+im,winW,winH,step)
    
    optional: not recommended : when non-operlapping is used via 'view_as_blocks'
        
        example1: slide_patch,img_block,x_indices,y_indices=extract_patch(data_path+im,winW,winH,step)
    
    """
    
    if type(im) is not np.ndarray:
        im = np.array(PIL.Image.open(im))
        offset=30
        im=im[offset:im.shape[0]-offset-1,offset:im.shape[0]-offset-1]
        im= im.astype('float32')
        #blk = view_as_blocks(im, block_shape=(winW, winH)) # non-overlapping block
        xx=[]
        yy=[]
        win=[]
        patch_list=[]
        for y in range(0, im.shape[0],step):
            for x in range(0, im.shape[1],step):
    			# yield the current window
                   mm=im[y:y + winW, x:x + winH]
                   if mm.shape[0] != winH or mm.shape[1] != winW:
                       continue
                   win.append(mm) #
                   xx.append(x)  
                   yy.append(y)
        #tmp1=overlapPatch_list(win,winW,winH)
        #[patch_list.append(tmp1[i]) for i in range(len(tmp1))]\
        patch_list=win     
    else:
        offset=30
        im=im[offset:im.shape[0]-offset-1,offset:im.shape[0]-offset-1]
        im= im.astype('float32')
        #blk = view_as_blocks(im, block_shape=(winW, winH)) # non-overlapping block
        xx=[]
        yy=[]
        win=[]
        patch_list=[]
        for y in range(0, im.shape[0],step):
            for x in range(0, im.shape[1],step):
    			# yield the current window
                   mm=im[y:y + winW, x:x + winH]
                   if mm.shape[0] != winH or mm.shape[1] != winW:
                       continue
                   win.append(mm) #
                   xx.append(x)  
                   yy.append(y)
        patch_list=win      
    return patch_list, xx, yy #blk,#lst,

def  overlapPatch_list(tmp,winW,winH):
    
    """
    numpy array of patches 
    
    Input:
        tmp: output of extract_patch
        
        example: data_array=overlapPatch_list(slide_patch)
    """

    tmp1=np.asarray([tmp[pat] for pat in range(len(tmp))])
    normalized_data=preprocesser(tmp1,winW,winH)#preprocess
    return normalized_data

def overlap_Patch_stitch(im,winW,winH,step):
        
    """
    stiching patches to make the full size original image
    
    Input:
        total_patch: output of extract_patch
        win_dim= window height or window wirdth 
        x_ind= x dimension indices: output from extract_patch
        y_ind= y dimension indices: output from extract_patch    
    Output: stiched image of full size 
    
    example: Original_stitch=overlap_Patch_stitch(slide_patch,winH,x_indices,y_indices)
    """
    win_dim=winW
    total_patch,x_ind,y_ind=extract_patch(im,winW, winH,step)
    tmp1=np.zeros((win_dim,im.shape[0]),dtype=np.float32)
    tmp2=np.zeros((im.shape[0],im.shape[1]), dtype=np.float32)
    for r in range(0,len(total_patch),np.int(math.sqrt((len(total_patch))))):
        for i in range(0,np.int(math.sqrt((len(total_patch))))):
            tmp3=total_patch[r+i]
            tm=x_ind[r+i]
            rt=y_ind[r+i]
            tmp1[0:,tm:tm+win_dim]=tmp3
            tmp2[rt:rt+win_dim,0:]=tmp1
    return tmp2

'''
#============== utilities for view_as_block part (optional)=====#

def  non_overlapPatch_list(tmp):

    """
    addition to make a list of all patches when blk is return from extract_patch

    not used in current work
    
    example: patch_list, indi_list,data_array_ta=non_overlapPatch_list(img_block)
    """

    xx=[]
    zz=[]
    for i in range(tmp.shape[0]):
        for ii in range(tmp.shape[1]):
            tt=tmp[i,ii,:,:]
            xx.append(tt)
            yy=[i,ii]
            zz.append(yy)
    tmp=np.concatenate([pat[np.newaxis, ...] for pat in xx], axis=0)
    return xx,zz, tmp

def non_overlap_Patch_stitch(y,tmp,batch):
    
    """
    stiching patches to make the full size original image
    
    not used in currernt work
    
    example: Original_stitch=non_overlap_Patch_stitch(img_block,patch_list,step)
    """
    xx=[]
    for i in range(0,y.shape[1]*y.shape[0],y.shape[1]):#y.shape[0]):
        tmp1=np.zeros((batch,2048),dtype=np.uint16)
        tmp2=np.zeros((2048,2048), dtype=np.uint16)
        for ii in range(y.shape[1]):
            tmp3=tmp[ii+i]
            tmp4=ii*batch
            tmp1[0:,tmp4:tmp4+batch]=tmp3
        xx.append(tmp1)
        for iii in range(len(xx)):
            tm=xx[iii]
            tmp5=iii*batch
            tmp2[tmp5:tmp5+batch,0:]=tm
    return tmp2
'''
#=======================utiltites for training data prepration==================#

def first_4chars(x):
	return(x[4:])

def find_files(path_to_dir, suffix=".tif\n"):
	ff=[]
	filename=os.listdir(path_to_dir)
	[filename[files] for files in range(len(filename)) if filename[files].endswith(suffix) ]
	filess=sorted(filename, key=first_4chars)
	for i in range(len(filess)):
		if not filess[i].startswith('.'):
			ff.append(filess[i]) 
	return ff#iless

def h5_write(dataset, mode):
    h5_file = h5py.File(os.getcwd()+'/train_data/'+str(mode)+'.h5', 'w')
    dataset_h5 = h5_file.create_dataset(str(mode), data=dataset)
    return dataset_h5   
    
def data_extract(path_to_dir,range1,range2,winW,winH,step, mode):

    #====for training data 
    
    if mode=="train":
        print("training_data")
        d=[]
       
        f_list=find_files(path_to_dir)
        for i in range(range1,range2):
            im=f_list[i]
            slide_patch,x_indices,y_indices=extract_patch(path_to_dir+im,winW,winH,step)
            data_array=overlapPatch_list(slide_patch,winW,winH)
            d.append(data_array)
            batch=len(slide_patch)
        tmp1=np.zeros((batch*len(d),winW, winH),dtype='float32')

        for ii in range(len(d)):
            t=ii*batch
            tmp=d[ii]
            tmp1[t:(batch)+(t),:,:]=tmp

    #====for groundtruth data
    elif mode=="GT": 
        print("GT_data")
        gt=[]
        f_list=find_files(path_to_dir)
        for i in range(range1,range2):
            im=f_list[i]
            slide_patch,x_indices,y_indices=extract_patch(path_to_dir+im,winW,winH,step)
            data_array=overlapPatch_list(slide_patch,winW,winH)
            gt.append(data_array)
            batch=len(slide_patch)
        tmp1=np.zeros((batch*len(gt),winW, winH),dtype='float32')

        for ii in range(len(gt)):
            t=ii*batch
            tmp=gt[ii]
            tmp1[t:(batch)+(t),:,:]=tmp
# =============================================================================
#         gt_slide_patch,x_indices,y_indices=extract_patch(path_to_dir,winW,winH,step)
#         gt_data_array=overlapPatch_list(gt_slide_patch,winW,winH)
#         for i in range(range2-range1):
#             gt.append(gt_data_array)
#             batch=len(gt_slide_patch)
#         tmp1=np.zeros((batch*len(gt),winW, winH),dtype='float32')
#         for ii in range(len(gt)):
#             t=ii*batch
#             tmp=gt[ii]
#             tmp1[t:(batch)+(t),:,:]=tmp
# =============================================================================
        
    #====patches for testing                 
    elif mode=="test":
        print("test_data")
        d=[]
       
 
        f_list=find_files(path_to_dir)
        for i in range(range1,range2):
            im=f_list[i]
            slide_patch,x_indices,y_indices=extract_patch(path_to_dir+im,winW,winH,step)
            data_array=overlapPatch_list(slide_patch,winW,winH)
            d.append(data_array)
            batch=len(slide_patch)
        tmp1=np.zeros((batch*len(d),winW, winW),dtype='float32')

        for ii in range(len(d)):
            t=ii*batch
            tmp=d[ii]
            tmp1[t:(batch)+(t),:,:]=tmp
             
    return tmp1


#================folds training and testing during cross validation 
def create_file_indices(file_type):

    fileName = os.getcwd()+'/train_data/'+file_type +".h5"
    
    f=h5py.File(fileName,  "r")
    
    tmp1= f[file_type]
    tmp2=[]
    
    for iii in range(tmp1.shape[0]):
            
        tmp2.append(iii)  

    dtype = [('h5_id','int32')]
             
    values = np.zeros(len(tmp2), dtype=dtype)
    
    index = [str(i) for i in range(len(values))]
    df1 = pd.DataFrame(values, index=index)
    
    df1['h5_id']=df1.index

    df=df1.reindex(np.random.permutation(df1.index))
    
    df11 = df.iloc[:len(df)//8,:]
                   
    df22=df.iloc[(len(df)//8):,:]
                 
    file_tss=open(os.getcwd()+'/train_data/'+'test_split.csv','w')
    
    file_trs=open(os.getcwd()+'/train_data/'+'train_split.csv','w')
    
    file_trs.write(str('h5_id')+  "\n")
    
    file_tss.write(str('h5_id')+ "\n")
    
    for ii in range(len(df11)):
        
        p=df11['h5_id']

        id1=p[ii]

        file_tss.write(str(id1)+"\n")   
    file_tss.close()# file for indpendent test of each fold with its corrosponding labels
    
    for ii in range(len(df22)):
        
        p=df22['h5_id']

        id1=p[ii]


        file_trs.write(str(id1)+ "\n")
        
    file_trs.close()# file for 5 fold cross validation

    return  file_trs, file_tss

        
def fold_file(data_file,fold_set, fold):
    
    for i in range(len(fold_set)):

            file=open(os.getcwd()+'/train_data/'+'fold_'+str(fold) + str(i+1) + '.csv','w')

            file.write(str('h5_id')+ "\n")
            
            tmm=fold_set[i]

            for ii in range(len(tmm)):

                p=data_file['h5_id']

                id1=p[tmm[ii]]


                file.write(str(id1)+ "\n")
                
            if fold =="tr":
                
                print ("training fold generated: "+ str(i+1)+ " total sample: " + str(ii+1)+ "\n")
                
            elif fold =="val":
                
                print ("validation fold generated: "+ str(i+1)+ " total sample: " + str(ii+1)+ "\n")
            
                file.close()        
             
def cv_fold(file_trs):

    f = pd.read_csv(os.path.join(os.getcwd()+'/train_data/'+ (file_trs)))

    kf = KFold(len(f), n_folds=5, shuffle=True, random_state=seed)
    X_tr=[]

    X_val=[]

    for train, val in kf:
        X_tr.append(train)

        X_val.append(val)

    xx_train=fold_file(f,X_tr,"tr")
       
    xx_val=fold_file(f,X_val,"val")
    
    return xx_train, xx_val        
   
def image_chunk(path,winW,winH,step,mode,range1,range2,range3=None,range4=None,range5=None,range6=None):
    
    if range3 and range4 is not None:
        print ("data extraction from second range")
        if range5 and range6 is not None:
            print ("data extraction from third range")
            set1=data_extract(path,range1,range2,winW,winH,step,mode=mode) 
            set2=data_extract(path,range3,range4,winW,winH,step,mode=mode)
            set3=data_extract(path,range5,range6,winW,winH,step,mode=mode) 
            data=np.concatenate((set1,set2,set3),axis=0)
            h5_write(data, mode=mode)
        else:
            print ("data extraction from first and second range")
            set1=data_extract(path,range1,range2,winW,winH,step,mode=mode) 
            set2=data_extract(path,range3,range4,winW,winH,step,mode=mode)
            data=np.concatenate((set1,set2),axis=0)
            h5_write(data, mode=mode)

    else:
        print ("data extraction from first range")
        set1=data_extract(path,range1,range2,winW,winH,step,mode=mode) 
        data=set1
        h5_write(data, mode=mode)

    return data

#=============prediction part==================

    
def predict_normalize(img,val):
    imgout = np.float32(img)
    imgout =imgout/val
    return(imgout)

def norm(im):
    m0=np.zeros(im.shape[0],im.shape[0])
    s0=np.zeros(im.shape[0],im.shape[0])
    m0[:]=361.2110
    s0[:]=35.9486
    a=np.zeros(im.shape[0],im.shape[0])
    b=np.zeros(im.shape[0],im.shape[0])
    a[:]=146.1705
    b[:]=438.6816
    m=np.zeros(im.shape[0],im.shape[0])
    s=np.zeros(im.shape[0],im.shape[0])
    m[:]=np.mean(im)
    s[:]=np.std(im)
    imm=((im-m)/(s*s0+m0))
    tt=np.min(imm,b)
    y=np.max(a,tt)
    return y

def pix_weights(size1,sigma):
    tmp = np.zeros((size1,size1),dtype='float32')
    mid = np.ceil(size1/2);
    sig = np.floor((size1/2)/sigma)
    for i in range(size1):
        for j in range(size1):
            d=np.sqrt(((i-mid)**2)+((j-mid)**2))
            tmp[i,j] = np.exp((-d**2)/(2*(sig**2))) / (sig*np.sqrt(2*np.pi))
    tmp = tmp/np.max(tmp)
    return tmp

def prediction_full_image(im, winW,winH,step,val=650.0, weightsSig=None,model_name=None):
    
    
    model =load_model(os.getcwd()+'/trained_models/'+str(model_name)+'.h5',custom_objects={'custom_loss': custom_loss})
   
    patchSz= np.int(np.sqrt((winW*winH)))
    patchSzOut = np.int(np.sqrt(winW*winH))
    p_diff = np.int((patchSz - patchSzOut)/2)
    im=predict_normalize(im,val)
    sz = im.shape[0]

    if (p_diff>0):
    
        im=np.concatenate((np.fliplr(im[:,:p_diff]),im,(np.fliplr(im[:,sz-p_diff:sz]))),axis=1)
        im=np.concatenate((np.flipud(im[:p_diff,:]),im,(np.flipud(im[sz-p_diff:sz,:]))),axis=0)
    pixel_weights=pix_weights(patchSzOut,weightsSig)
    range_y = list(range(0, np.int(im.shape[1]-patchSz), step))
    range_x = list(range(0, np.int(im.shape[0]-patchSz), step))
    if range_y[-1] !=np.int(im.shape[1]-patchSz):
        range_y.append((np.int(im.shape[1]-patchSz)))
    if range_x[-1] !=np.int(im.shape[0]-patchSz):
        range_x.append((np.int(im.shape[1]-patchSz)))
        
    chunkSize=len(range_x)
    res=[]
    positions_out =[]
    positions_out1 =[]
    denoisedIm = np.zeros((sz,sz), dtype='float32')
    wIm = np.zeros((sz,sz), dtype='float32')

    for y in range(len(range_y)):
        for x in range(len(range_x)):
            p=np.transpose(im[range_y[y]:range_y[y]+patchSzOut,range_x[x]:range_x[x]+patchSzOut])
            procimg = p[np.newaxis,:,:,np.newaxis]
            imgout_orig = model.predict(procimg)#imgout_orig1
            imgout = imgout_orig.reshape(patchSzOut,patchSzOut)
            res.append(np.reshape(imgout, (patchSzOut*patchSzOut),1))
            positions_out.append(range_y[y])
            positions_out1.append(range_x[x])
            if x==chunkSize-1:
                patches_w =(np.matlib.repmat(np.reshape(pixel_weights, patchSzOut*patchSzOut,1),chunkSize,1))
                p_final =np.multiply(res,(patches_w))
                for j in range(chunkSize):
                    denoisedIm[positions_out[j]:positions_out[j]+patchSzOut,positions_out1[j]:positions_out1[j]+patchSzOut]= denoisedIm[positions_out[j]:positions_out[j]+patchSzOut,positions_out1[j]:positions_out1[j]+patchSzOut] +np.reshape(p_final[j,:], (patchSzOut,patchSzOut))
                    wIm[positions_out[j]:positions_out[j]+patchSzOut,positions_out1[j]:positions_out1[j]+patchSzOut] = wIm[positions_out[j]:positions_out[j]+patchSzOut,positions_out1[j]:positions_out1[j]+patchSzOut] + np.reshape(patches_w[j,:], (patchSzOut,patchSzOut))
        res=[] 
        positions_out =[]
        positions_out1 =[]               
    cm=np.divide(denoisedIm,wIm)
    cm=cm*val
    cm=norm(cm)
    return cm


#=============plotting utilites======================#

def plot_patches(patches, fignum=None, low=0, high=0):
    
    """
    Given a stack of 2D patches indexed by the first dimension, plot the
    patches in subplots. 
    'low' and 'high' are optional arguments to control which patches
    actually get plotted. 'fignum' chooses the figure to plot in.
    """
    try:
        istate = plt.isinteractive()
        plt.ioff()
        if fignum is None:
            fig = plt.gcf()
        else:
            fig = plt.figure(fignum)
        if high == 0:
            high = len(patches)
        #pmin, pmax = patches.min(), patches.max()
        dims = np.ceil(np.sqrt(high - low))
        for idx in range(high - low):
            pmin, pmax = patches[idx].min(), patches[idx].max()
            spl = plt.subplot(dims, dims, idx + 1)
            ax = plt.axis('off')
            im = plt.imshow(patches[idx], cmap=matplotlib.cm.gray)
            cl = plt.clim(pmin, pmax)
        plt.show()
    finally:
        plt.interactive(istate)

       
def plot_image(tmp):
    
    """
    plot full size image
    
    """
    plt.imshow(tmp, cmap=matplotlib.cm.gray)

def show_imgs(x_test,winW,winH, decoded_imgs=None, n=10):
    
    """
    display image_patches after testing with trained denoising model
    
    
    """
    plt.figure(figsize=(20, 4))
    for i in range(n):
        ax = plt.subplot(2, n, i+1)
        plt.imshow(x_test[i].reshape(winW,winH))
        pmin, pmax = x_test[i].min(), x_test[i].max()
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        cl = plt.clim(pmin, pmax)

        if decoded_imgs is not None:
            ax = plt.subplot(2, n, i+ 1 +n)
            plt.imshow(decoded_imgs[i].reshape(winW,winH))
            pmin, pmax = decoded_imgs[i].min(), decoded_imgs[i].max()
            plt.gray()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)
            cl = plt.clim(pmin, pmax)
    plt.show()    