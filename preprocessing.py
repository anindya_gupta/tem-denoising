#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 13:09:14 2017

@author: anindya
"""
import numpy as np
from skimage.transform import resize

#================normalization and preprocess===================#

def preprocess(imgs,img_rows, img_cols):
    imgs_p = np.ndarray((imgs.shape[0], img_rows, img_cols))
    for i in range(imgs.shape[0]):
        imgs_p[i] = resize(imgs[i], (img_cols, img_rows), preserve_range=True)

    #imgs_p = imgs_p[..., np.newaxis]
    imgout = np.float32(imgs_p)
    imgout = imgout-imgout.min()
    imgout = (imgout/imgout.max())
    #imgs_train /= 255
    return imgout

def preprocesser(imgs,img_rows, img_cols):
    imgs_p = np.ndarray((imgs.shape[0], img_rows, img_cols))
    for i in range(imgs.shape[0]):
        imgs_p[i] = resize(imgs[i], (img_cols, img_rows), preserve_range=True)

    imgs_train = imgs_p.astype('float32')

    imgs_train =imgs_train/np.max(imgs_train)

    return imgs_train


def prepare_trainingData(imgs,img_rows, img_cols):
    imgs_p = np.ndarray((imgs.shape[0], img_rows, img_cols))
    for i in range(imgs.shape[0]):
        imgs_p[i] = resize(imgs[i], (img_cols, img_rows), preserve_range=True)
    imgs_p = imgs_p[..., np.newaxis]
    imgs_train = imgs_p.astype('float32')
    return imgs_train

def normalize(img):
    imgout = np.float32(img)
    imgout = imgout-imgout.min()
    imgout = (imgout/imgout.max())
    imgout = imgout-np.median(imgout)
    return(imgout)

def normalizetouint8(img):
    imgout = np.float32(img)
    imgout = imgout-imgout.min()
    imgout = (imgout/imgout.max())*255.0
    #    imgout = imgout-np.median(imgout)
    #    imgout = imgout - 1.0
    return(np.uint8(imgout))

def preProcess(imgs,img_rows, img_cols):
    imgs_p = np.ndarray((imgs.shape[0], img_rows, img_cols))
    for i in range(imgs.shape[0]):
        imgs_p[i] = imgs[i].reshape(32,32)

    #imgs_p = imgs_p[..., np.newaxis]
    imgs_train = imgs_p.astype('float32')
    return imgs_train
