#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 23 02:00:03 2017

@author: anindya
"""
from __future__ import absolute_import
import os
#os.makedirs(os.getcwd()+'/logs',exist_ok=True)
#os.makedirs(os.getcwd()+'/trained_models',exist_ok=True)
from keras.layers.normalization import BatchNormalization
import numpy as np
import pandas as pd
import h5py
import keras.backend as K
from keras import optimizers
#from keras.losses import 
from keras.models import load_model
from keras.callbacks import EarlyStopping, CSVLogger
from keras.utils import plot_model
from keras.models import Model
from keras.layers import Conv2D, Input, Activation,Deconvolution2D,UpSampling2D,MaxPooling2D#,# ZeroPadding2D
#from keras.optimizers import SGD
from keras.layers.merge import add, average#,subtract,dot,maximum
from augment_fly import data_augment	
import tensorflow as tf
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.75)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
import keras.losses
from keras.utils.generic_utils import get_custom_objects

#==================batch generator=============

def data_Genrator(fold_file,img_rows, img_cols, channel, batch,mode):
    
    # fold files reading:
    data = pd.read_csv(fold_file)
    ID1 = data.pop('h5_id')
    idx =0
    if mode=="DCNN":
        #print ("single net is ready for training")
        while 1:
    
            tmp=np.zeros((batch, img_rows, img_cols, channel))
            
            y = np.zeros((batch, img_rows, img_cols, channel))
         #   y1 = np.zeros((batch, img_rows, img_cols, channel))
           
            for i in range(batch):
    
                ii=idx*batch+(i) 
                
                ii = ii % (len(ID1)-10) # to track back to the starting image pointer
                
                data_1=[]
                label_1=[]
                
                fileName = os.getcwd()+'/train_data/'+"train.h5"
                f=h5py.File(fileName,'r')
                data_file= f['train']
                
                
                data_1.append(np.stack([data_file[ID1[ii]]],axis=-1))
                
                
                fileName1 = os.getcwd()+'/train_data/'+"GT.h5"
                f1=h5py.File(fileName1,'r')
                label_file= f1['GT']                
                label_1.append(np.stack([label_file[ID1[ii]]],axis=-1))#.reshape((20,20,7))]
                
                tp=np.asarray(data_1)
                tp1=np.asarray(label_1)
    
    
                tmp[i, :, :,:]=tp
                y[i, :, :,:]=tp1
    
            idx=idx+1
            yield tmp, y#[y,y1]
            



#===================CNN networks======================================#

#====================multiple outout net=======================
def Res_block():
    _input = Input(shape=(None, None, 32))

    conv = Conv2D(filters=16, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(_input)
    b_rc = BatchNormalization(name='batch_norm_r_c')(conv)
    conv = Conv2D(filters=32, kernel_size=(3, 3), strides=(1, 1), padding='same', activation='relu')(b_rc)
    b_rc1 = BatchNormalization(name='batch_norm_r_c1')(conv)
    out = add(inputs=[_input, b_rc1])
    out = Activation('relu')(out)

    model = Model(inputs=_input, outputs=out)

    return model


    
#=====================================single output network=====================#
    
def sub_net_1():
    _input = Input(shape=(None, None, 1),name='input')  # adapt this if using `channels_first` image data format
    
    level1_1 = Conv2D(16, (3, 3), padding='same',name='conv_1')(_input)#,activity_regularizer=regularizers.l1(10e-5))(_input)
    b1_1 = BatchNormalization(name='batch_norm_1')(level1_1)
    Act_1 = Activation('relu',name='ReLU_1')(b1_1)
  
    level1_2 = Conv2D(32, (3, 3), padding='same',name='conv_2')(Act_1)#,activity_regularizer=regularizers.l1(10e-5))(_input)
    b1_2 = BatchNormalization(name='batch_norm_2')(level1_2)
    Act1_2 = Activation('relu',name='ReLU_2')(b1_2)
    
    Feature_out = Res_block()(Act1_2)
    b1_3 = BatchNormalization(name='batch_norm_3')(Feature_out)
    
    level1_3 = Deconvolution2D(32, (3, 3),  padding='same', name='deconv_1')(b1_3)#,activity_regularizer=regularizers.l1(10e-3))(b_4)
    b1_4 = BatchNormalization(name='batch_norm_4')(level1_3)
    Act1_4 = Activation('relu',name='ReLU_3')(b1_4)
    
    level2 = add([b1_3, Act1_4])
    b2_1 = BatchNormalization(name='batch_norm_5')(level2)
    
    level2_1 = Conv2D(64, (3, 3), padding='same',name='conv_3')(b2_1)#,activity_regularizer=regularizers.l1(10e-5))(_input)
    b2_2 = BatchNormalization(name='batch_norm_6')(level2_1)
    Act2_1 = Activation('relu',name='ReLU_4')(b2_2)
    
    level3 = add([_input, Act2_1])
    b3_1 = BatchNormalization(name='batch_norm_7')(level3)
    
    level3_1 = Deconvolution2D(64, (3, 3),  padding='same', name='deconv_2')(b3_1)#,activity_regularizer=regularizers.l1(10e-3))(b_4)
    b3_1 = BatchNormalization(name='batch_norm_8')(level3_1)
    Act3_1 = Activation('relu',name='ReLU_5')(b3_1)
    
    decoded = Conv2D(1, (5, 5), padding='same', name='conv_4')(Act3_1)#,kernel_regularizer=regularizers.l2(10e-3))(b_6)
    b4_1 = BatchNormalization(name='batch_norm_9')(decoded)
    Act4_1 = Activation('linear',name='linear_1')(b4_1)
    model = Model(input=_input,  outputs=Act4_1)
    return model
    
def sub_net_2():    
    _input = Input(shape=(None, None, 1),name='input') 
    level4_1 = Conv2D(32, (3, 3), padding='same',name='conv_5')(_input)#,activity_regularizer=regularizers.l1(10e-5))(_input)
    b4_1 = BatchNormalization(name='batch_norm_10')(level4_1)
    Act4_1 = Activation('relu',name='ReLU_6')(b4_1)
    
    level4_2 = MaxPooling2D((2, 2), padding='same',name='pool_1')(Act4_1)#,activity_regularizer=regularizers.l1(10e-5))(_input)
    
    Feature_out1 = Res_block()(level4_2)
    b4_2 = BatchNormalization(name='batch_norm_11')(Feature_out1)
        
    level5_1 = Conv2D(32, (3, 3), padding='same',name='conv_6')(b4_2)#,activity_regularizer=regularizers.l1(10e-5))(_input)
    b5_1 = BatchNormalization(name='batch_norm_12')(level5_1)
    Act5_1 = Activation('relu',name='ReLU_7')(b5_1)
    
    level5_1 = MaxPooling2D(32,(2, 2), padding='same',name='pool_2')(Act5_1)
    
    level6_1 = Conv2D(32, (3, 3), padding='same',name='conv_7')(level5_1)#,activity_regularizer=regularizers.l1(10e-5))(_input)
    b6_1 = BatchNormalization(name='batch_norm_14')(level6_1)
    Act6_1 = Activation('relu',name='ReLU_8')(b6_1)
    
    x = UpSampling2D((2, 2),name='upsampling_1')(Act6_1)
    
    level7_1 = Conv2D(32, (3, 3), padding='same',name='conv_8')(x)#,activity_regularizer=regularizers.l1(10e-5))(_input)
    b7_1 = BatchNormalization(name='batch_norm_15')(level7_1)
    Act7_1 = Activation('relu',name='ReLU_9')(b7_1)
    
    x = UpSampling2D((2, 2),name='upsampling_2')(Act7_1)
    
    decoded_2 = Conv2D(1, (3, 3), padding='same',name='conv_9')(x)#linear
    b8_1 = BatchNormalization(name='batch_norm_16')(decoded_2)
    Act8_1 = Activation('sigmoid',name='sigmoid_1')(b8_1)
    model = Model(input=_input,  outputs=Act8_1)#
    return model

def custom_loss(y_true,y_pred):
    loss1=K.mean(K.square(y_pred-y_true),axis=-1)
    loss2=K.mean(K.binary_crossentropy(y_true,y_pred),axis=-1)
    return loss1+loss2


def ensemble():
    _input = Input(shape=(None, None, 1),name='input') 
    _model_1=sub_net_1()(_input)
    _model_2=sub_net_2()(_input)
    _merging=average(inputs=[_model_1, _model_2])
    
    model = Model(input=_input,  outputs=_merging)
    
    return model
 
def DCNN_training(data,nb_train_samples,val_data,nb_validation_samples, epoch,batch,i):
    print ("DCNN is ready for training")
    NET=ensemble()##single_net_2()#:ensemble()#ingle_net_1()
    learning_rate = 1e-3
    decay_rate = learning_rate / epoch
   
    sgd= optimizers.SGD(lr=learning_rate, decay=decay_rate, momentum=0.9, nesterov=True)


    NET.compile(optimizer=sgd, loss=custom_loss)
    if i==0:
        NET.summary()
        plot_model(NET, to_file=(os.getcwd()+'/trained_models/'+"DCNN.png"), show_shapes=True)
    
    csv_logger = CSVLogger(os.getcwd()+'/logs/'+'DCNN_log_'+str(i+1)+'.csv', append=True, separator=',')
    early_stopping = EarlyStopping(monitor='val_loss', patience=5)
    NET.fit_generator(data,
                      verbose=1,validation_data=val_data,
                      steps_per_epoch=nb_train_samples/batch,
                      epochs=epoch,
                      callbacks=[ csv_logger,early_stopping],
                      validation_steps=nb_validation_samples/batch)
                      
   #callbacks=[TensorBoard(log_dir='/tmp/autoencoder')])
    NET.save(os.getcwd()+'/trained_models/'+'DCNN_model_'+ str(i+1)+'.h5')
    NET.save_weights(os.getcwd()+'/trained_models/'+'DCNN_weights_'+ str(i+1)+'.h5')

