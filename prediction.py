#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 23:02:44 2017

@author: anindya
"""

import os
import numpy as np
import PIL
from PIL import Image
import time
from helpers import find_files, prediction_full_image



path=os.getcwd()+'/Data Denoising/HM/'
GT_path=os.getcwd()+'/Data Denoising/GT/'

f=find_files(path, suffix=".tif")
for i in range(len(f)):
        print (i)
        im=(path+'/'+f[i])
        
        im1=(GT_path+'/'+'gt_'+str(i+1)+'.tif')
        im1=np.array(PIL.Image.open(im1), dtype='float32')
        print (f[i])
        start = time.clock()
        im = np.array(PIL.Image.open(im))
        im= im.astype('float32')

        denoised_image=prediction_full_image(im, winW=128,winH=128,step=64, weightsSig=2,model_name='DCNN_model_1')

        ima = Image.fromarray(denoised_image)
        ima.save(os.getcwd()+'/result_images/'+'outputs_'+str(f[i]))
        end = time.clock()
        elapsed = end - start
        print("DCNN : %s s" % str(elapsed))
